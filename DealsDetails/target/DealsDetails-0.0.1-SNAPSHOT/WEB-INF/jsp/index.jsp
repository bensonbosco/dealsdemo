<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
	<head>
		<title>Bloomberg</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script>
			$(document).ready(function() {
				<c:if test="${empty faultDataLst}">
				$('#errorMsg').text("File already uploaded !!");
				</c:if>
			});
		</script>
	</head>
	<body>
<form method="post" action="DealsDetails.do">
<p>
Type some  text (if you like):<br>
<input type="text" name="textLine" id="textLine">
</p>
<p>
Please specify a file, or a set of files:<br>
<input id="input-b1" name="input-b1" type="file" class="file">
</p>
<p id="errorMsg" style="font:size:10px; color:red"></p>
<div>
<input type="submit" value="Send">
</div>
</form>
	</body>
</html>