package com.demo.deals.controller;


import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import com.demo.deals.model.FileDetails;
import com.demo.deals.service.DealsService;

public class DealsControllerTest {


	@Test
	public void testFileName(){
		DealsController dc = new DealsController();
		String input = "D:\\CSVFile\\MOCK_DATA_EX.csv";
		assertEquals("MOCK_DATA_EX.csv", dc.getFileName(input));
	}

	@Test
	public void testValidateDeals() {
		DealsController dc = new DealsController();
		String[] input = {"7","USD","RUB","2017-09-17 20:05:28","7.21"};
		assertTrue(dc.validate(input));
	}
	
	@Test
	public void testValidateInvalidDeals() {
		DealsController dc = new DealsController();
		String[] input = {"7","USD","XYZ","2017-09-17 20:05:28","7.21"};
		assertFalse(dc.validate(input));
	}

	@Test
	public void testValidateInvalidDate() {
		DealsController dc = new DealsController();
		String[] input = {"7","USD","","2017-09-17 345:05:28","7.21"};
		assertFalse(dc.validate(input));
	}
	
	@Test
	public void testCheckFile() {
		DealsService ds = new DealsService();
		FileDetails fd = new FileDetails();
		fd.setFileId(1);
		fd.setFileName("MOCK_DATA_EX.csv");
		fd.setNoOfDeals(100);
		fd.setNoOfInvalidDeals(5);
		fd.setStartTime(Timestamp.valueOf("2017-12-02 00:34:11"));
		fd.setEndTime(Timestamp.valueOf("2017-12-02 00:34:12"));
		assertEquals(fd, ds.searchFile(fd.getFileName()));
	}

	@Test
	public void testSearchFile() throws ServletException {
		DealsController dc = new DealsController();
		HttpServletRequest req = mock(HttpServletRequest.class);
		HttpServletResponse res = mock(HttpServletResponse.class);
		when(req.getParameter("searchFile")).thenReturn("MOCK_DATA_EX.csv");

		FileDetails fd = new FileDetails();
		fd.setFileId(1);
		fd.setFileName("MOCK_DATA_EX.csv");
		fd.setNoOfDeals(100);
		fd.setNoOfInvalidDeals(5);
		fd.setStartTime(Timestamp.valueOf("2017-12-02 00:34:11"));
		fd.setEndTime(Timestamp.valueOf("2017-12-02 00:34:12"));
		
		ModelAndView mav = dc.SearchFile(req, res);
		assertNotNull(fd);
		assertNotNull(mav);
		assertNotNull(mav.getModel().get("fileDetails"));
		assertEquals(fd, mav.getModel().get("fileDetails"));
		
		
	}
	
	@Test
	public void TestUploadFile() throws FileUploadException, ServletException, IOException {
		DealsController dc = new DealsController();
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		ServletContext servletContext = mock(ServletContext.class);
		when(request.getServletContext()).thenReturn(servletContext);
		when(request.getContentType()).thenReturn("multipart/form-data; boundary=someBoundary");
		assertNotNull(dc.DealsHome(request, response));
	}
	
	@Test
	public void testCreateValidDeals() {
		DealsController dc = new DealsController();
		String[] input = {"7","USD","","2017-09-17 345:05:28","7.21"};
		assertFalse(dc.validate(input));
	}
	
	
}
