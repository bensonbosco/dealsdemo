package com.demo.deals.dao;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.ArrayList;

import org.junit.Test;

import com.demo.deals.model.Deals;
import com.demo.deals.model.DealsInvalid;

public class DealsDaoTest {

	DealsDao dd = new DealsDao();
	
	
	@Test
	public void testAddFileDetails() {
		String fileName = "MOCK_DATA_EX.csv";
		String id ="2";
		Timestamp time = new Timestamp(System.currentTimeMillis());
		assertEquals(id, dd.addFileData(fileName, time));
		
	}
	
	@Test 
	public void testAddValidDeals(){
		ArrayList<Deals> valid = new ArrayList<>();
		valid.add(new Deals(1, "EUR", "CNY", Timestamp.valueOf("2017-02-28 12:17:44"), 5.93, "1"));
		valid.add(new Deals(2, "RUB", "USD", Timestamp.valueOf("2017-03-17 18:55:08"), 5.19, "1"));
		dd.addData(valid);
		assertEquals(2, valid.size());
	}
	
	@Test 
	public void testAddInvalidDeals(){
		ArrayList<DealsInvalid> inValid = new ArrayList<>();
		inValid.add(new DealsInvalid(1, "EUR", "CNY", Timestamp.valueOf("2017-02-28 12:17:44"), 5.93, "1"));
		inValid.add(new DealsInvalid(2, "RUB", "USD", Timestamp.valueOf("2017-03-17 18:55:08"), 5.19, "1"));
		dd.addInvalidData(inValid);
		assertEquals(2, inValid.size());
	}
	

}
