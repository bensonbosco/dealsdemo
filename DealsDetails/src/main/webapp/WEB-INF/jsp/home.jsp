<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
	<head>
		<title>Bloomberg</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script>
			
			$(document).ready(function() {
				
				<c:if test="${not empty file}">
					$('#errorMsg').text("file already exists !!");
					$('#fileDiv').hide();
				</c:if>
				<c:if test="${not empty fileId}">
					$('#SuccessMsg').text("file added successfully....");
				</c:if>

				<c:if test="${not empty fileName}">
					<c:if test="${empty fileDetails}">
						$('#errorSrchMsg').text("No Data found!!");
						$('#fileDiv').hide();
					</c:if>
					<c:if test="${not empty fileDetails}">
						$('#fileDiv').show();
					</c:if>
				</c:if>
			});

			function fileSearch() {
				if (!($('#searchFile').val().trim())) {
					$('#errorSrchMsg').text("Please enter file name !!");
					return;
				}

				$("#fileSearch").attr("action", "SearchFile.do");
				$("#fileSearch").attr("method", "post");
				$("#fileSearch").submit();
			}
		</script>
	</head>
	<body>
	<form id="fileUploadForm" action="DealsDetails.do" method="post" enctype="multipart/form-data">

		<p>
			<strong>Please select file to upload:</strong>
		</p>
		<input id="fileUpload" name="fileUpload" type="file" class="file" required>
		<p id="errorMsg" style="font: size:10px; color: red"></p>
		<p id="SuccessMsg" style="font: size:10px; color: green"></p>

		<div>
			<input type="submit" value="upload">
		</div>

	</form>
	<form id="fileSearch">
		<p >
			<strong>Search:</strong>
		</p>
		<input type="text" id="searchFile" name="searchFile" required>
		<div>
			<input type="submit" value="Search" onclick="fileSearch()">
			<p id="errorSrchMsg" style="font: size:10px; color: red"></p>
		</div>
	</form>
	<div class="panel panel-default" style="margin-top:10px; overflow:auto;" id="fileDiv">
			<table class="table table-fixedheader table-striped " style="height:355px">
		      <thead  style="background-color:#b7b7b7;">
				<tr>
	      			<th width="7%">File Name</th>
             		<th width="7%">Start time</th>
              		<th width="7%">End Time</th>
             		<th width="8%">Total Deals</th>
           			<th width="9%">Invalid Deals</th>
           			
	      		</tr>
	    	</thead>
	    	<tbody style="height: 323px">
                 	
                 	<tr>
                 	<td width="7%">${fileDetails.fileName}</td>
                   	<td width="7%">${fileDetails.startTime}</td>
                   	<td width="7%">${fileDetails.endTime}</td>
                   	<td width="8%">${fileDetails.noOfDeals}</td>
                   	<td width="9%">${fileDetails.noOfInvalidDeals}</td>
                   	
                	</tr>
                	
	    	</tbody>
			</table>
		</div>
</body>
</html>