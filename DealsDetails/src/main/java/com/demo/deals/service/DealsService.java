package com.demo.deals.service;

import java.sql.Timestamp;
import java.util.List;

import com.demo.deals.dao.DealsDao;
import com.demo.deals.model.Deals;
import com.demo.deals.model.DealsInvalid;
import com.demo.deals.model.FileDetails;

public class DealsService {
	DealsDao dealDao = new DealsDao();
	public void addData(List<Deals> dealsList){
		dealDao.addData(dealsList);
	}
	public String addFileData(String fileName, Timestamp startTime){
		return dealDao.addFileData(fileName,startTime);
	}
	public void addInvalidData(List<DealsInvalid> in) {
		dealDao.addInvalidData(in);
		
	}
	public FileDetails searchFile(String file) {
		return dealDao.searchFile(file);
		
	}
	
}
