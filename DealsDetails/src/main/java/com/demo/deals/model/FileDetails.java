package com.demo.deals.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="file_details")
public class FileDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int fileId;
	private String fileName;
	private Timestamp startTime;
	private Timestamp endTime;
	private int noOfDeals;
	private int noOfInvalidDeals;
	
	public FileDetails(){
		
	}
	
	public FileDetails(int fileId){
		this.fileId=fileId;
	}
	
	public int getFileId() {
		return fileId;
	}
	public void setFileId(int fileId) {
		this.fileId = fileId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public int getNoOfDeals() {
		return noOfDeals;
	}

	public void setNoOfDeals(int noOfDeals) {
		this.noOfDeals = noOfDeals;
	}

	public int getNoOfInvalidDeals() {
		return noOfInvalidDeals;
	}

	public void setNoOfInvalidDeals(int noOfInvalidDeals) {
		this.noOfInvalidDeals = noOfInvalidDeals;
	}
	
}
