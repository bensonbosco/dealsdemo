package com.demo.deals.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbldealsinvalid")
public class DealsInvalid {
	
	@Id
	private int id;
	private String fromCurrencyCode;
	private String toCurrencyCode;
	private Timestamp dealTimeStamp;
	private double dealAmount;
	private String fileNameId;
	
	public DealsInvalid(){
		
	}
	
	public DealsInvalid(int id, String fromCurrencyCode, String toCurrencyCode, Timestamp dealTimeStamp, double dealAmount,
			String fileNameId) {
		this.id = id;
		this.fromCurrencyCode = fromCurrencyCode;
		this.toCurrencyCode = toCurrencyCode;
		this.dealTimeStamp = dealTimeStamp;
		this.dealAmount = dealAmount;
		this.fileNameId = fileNameId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFromCurrencyCode() {
		return fromCurrencyCode;
	}
	public void setFromCurrencyCode(String fromCurrencyCode) {
		this.fromCurrencyCode = fromCurrencyCode;
	}
	public String getToCurrencyCode() {
		return toCurrencyCode;
	}
	public void setToCurrencyCode(String toCurrencyCode) {
		this.toCurrencyCode = toCurrencyCode;
	}
	public Timestamp getDealTimeStamp() {
		return dealTimeStamp;
	}
	public void setDealTimeStamp(Timestamp dealTimeStamp) {
		this.dealTimeStamp = dealTimeStamp;
	}
	public double getDealAmount() {
		return dealAmount;
	}
	public void setDealAmount(double dealAmount) {
		this.dealAmount = dealAmount;
	}
	public String getFileNameId() {
		return fileNameId;
	}
	public void setFileNameId(String fileNameId) {
		this.fileNameId = fileNameId;
	}
}
