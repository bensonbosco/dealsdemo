package com.demo.deals.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.demo.deals.controller.DealsController;
import com.demo.deals.model.Deals;
import com.demo.deals.model.DealsInvalid;
import com.demo.deals.model.FileDetails;

public class DealsDao {
	final static Logger logger = Logger.getLogger(DealsDao.class);
	public EntityManager entitymanager;
	public String fileId="";
	public void addData(List<Deals> dealsList){
		logger.info("Inside the method addData, for adding valid data :: stard");
		try{  
			DealsDao deal=new DealsDao();
			deal.getEntityManager();
			deal.doInsert(dealsList);
		}
		catch(Exception e){ 
			System.out.println(e);
		}
		logger.info("Inside the method addData, for adding valid data :: end");
	}
	private void doInsert(List<Deals> dealsList) {
		if (null != entitymanager) {
			
			EntityTransaction transaction = entitymanager.getTransaction();
			transaction.begin();
			for(Deals d: dealsList){
				Deals dls=new Deals();
				dls.setId(d.getId());
				dls.setFromCurrencyCode(d.getFromCurrencyCode());
				dls.setToCurrencyCode(d.getToCurrencyCode());
				dls.setDealTimeStamp(d.getDealTimeStamp());
				dls.setDealAmount(d.getDealAmount());
				dls.setFileNameId(d.getFileNameId());
				entitymanager.persist(dls);
				fileId=dls.getFileNameId();
			}
			transaction.commit();
			System.out.println("The object is persisted...");
			transaction.begin();
			Query query = entitymanager.createQuery("UPDATE FileDetails fd SET fd.endTime = ?1,fd.noOfDeals = ?2 WHERE fd.fileId = ?3");
			query.setParameter(1,new Timestamp(System.currentTimeMillis()));
			query.setParameter(2,dealsList.size());
			query.setParameter(3,Integer.parseInt(fileId));
			int updateCount = query.executeUpdate();
			if (updateCount > 0) {
				System.out.println("Done...");
			}
			transaction.commit();
			System.out.println("Valid Data Uploaded Sucessfully");
		}
		
	}
	private void getEntityManager() {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("DealsDetails");
		entitymanager = entityManagerFactory.createEntityManager();	
	}
	
	public String addFileData(String fileName, Timestamp startTime){
		EntityManager entitymanager;
		logger.info("Inside the method addFileData, for adding file Details :: start");
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("DealsDetails");
		entitymanager = entityManagerFactory.createEntityManager();
		System.out.println(entitymanager);
		if (null != entitymanager) {
			
			EntityTransaction transaction = entitymanager.getTransaction();
			
			FileDetails fd = new FileDetails();
			fd.setFileName(fileName);
			fd.setStartTime(startTime);
			fd.setEndTime(null);
			fd.setNoOfDeals(0);
			fd.setNoOfInvalidDeals(0);
			transaction.begin();
			entitymanager.persist(fd);
			transaction.commit();
			
			//entitymanager.flush();
			System.out.println(String.valueOf(fd.getFileId()));
			logger.info("Inside the method addFileData, for adding file Details :: end");
			return String.valueOf(fd.getFileId());

		}
		logger.info("Inside the method addFileData, for adding file Details :: end");
		return null;
	}
	public void addInvalidData(List<DealsInvalid> in) {
		logger.info("Inside the method addInvalidData, for adding Invalid data :: start");
		try{  
			DealsDao deal=new DealsDao();
			deal.getEntityManager();
			deal.doInsertInvalid(in);
		}
		catch(Exception e){
			System.out.println(e);
		}
		logger.info("Inside the method addInvalidData, for adding Invalid data :: end");
	}
	private void doInsertInvalid(List<DealsInvalid> in) {
		if (null != entitymanager) {
			EntityTransaction transaction = entitymanager.getTransaction();
			transaction.begin();
			for(DealsInvalid d: in){
				DealsInvalid dls=new DealsInvalid();
				dls.setId(d.getId());
				dls.setFromCurrencyCode(d.getFromCurrencyCode());
				dls.setToCurrencyCode(d.getToCurrencyCode());
				dls.setDealTimeStamp(d.getDealTimeStamp());
				dls.setDealAmount(d.getDealAmount());
				dls.setFileNameId(d.getFileNameId());
				entitymanager.persist(dls);
				fileId=dls.getFileNameId();
			}
			transaction.commit();
			transaction.begin();
			Query query = entitymanager.createQuery("UPDATE FileDetails fd SET fd.endTime = ?1,fd.noOfDeals = fd.noOfDeals + ?2,fd.noOfInvalidDeals = ?3 WHERE fd.fileId = ?4");
			query.setParameter(1,new Timestamp(System.currentTimeMillis()));
			query.setParameter(2,in.size());
			query.setParameter(3,in.size());
			query.setParameter(4,Integer.parseInt(fileId));
			int updateCount = query.executeUpdate();
			if (updateCount > 0) {
				System.out.println("Done...");
			}
			transaction.commit();
			System.out.println("Invalid Data Uploaded Sucessfully");
		}
	}
	public FileDetails searchFile(String file) {
		logger.info("Inside the method searchFile, for search file Details :: start");
		try{  
			DealsDao deal=new DealsDao();
			deal.getEntityManager();
			EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("DealsDetails");
			entitymanager = entityManagerFactory.createEntityManager();	
			if (null != entitymanager) {
				Query query = entitymanager.createQuery("Select f from FileDetails f where f.fileName like ?1");
				query.setParameter(1, file);
				List<FileDetails> list = query.getResultList();
				if(list.size()>0){
					logger.info("Inside the method searchFile, for search file Details :: start");
					return list.get(0);
				}
				
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		return null;
	}
}
