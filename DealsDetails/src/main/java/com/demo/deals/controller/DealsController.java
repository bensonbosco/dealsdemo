package com.demo.deals.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.demo.deals.model.Deals;
import com.demo.deals.model.DealsInvalid;
import com.demo.deals.model.FileDetails;
import com.demo.deals.service.DealsService;

@Controller
public class DealsController{
	final static Logger logger = Logger.getLogger(DealsController.class);
	
	String fileId = null;
	String filePath = null;
	DealsService dealsService = new DealsService();
	
	@RequestMapping(method=RequestMethod.POST, value="/DealsDetails")
	public ModelAndView DealsHome(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		logger.info("Inside the method DealsHome, url[/DealsDetails], http method[POST] :: Start");
		
		
		String msg="";
		ModelAndView mav = null;
		Timestamp startTime=new Timestamp(System.currentTimeMillis());
		this.uploadFile(request);
		FileDetails search = dealsService.searchFile(getFileName(filePath));
		if(search !=null){
			msg="File is already available";
			mav=new ModelAndView("home");
			mav.addObject("file", search);
		}
		else{
			msg="Successfully added";
			mav=new ModelAndView("home");
			mav.addObject("file", search);
			fileId=dealsService.addFileData(getFileName(filePath),startTime);
			readFromCSV(filePath);
			mav.addObject("fileId",fileId);
		}
		
		mav.addObject("Search", msg);
		logger.info("Inside the method DealsHome, url[/DealsDetails], http method[POST] :: end");
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/SearchFile")
	public ModelAndView SearchFile(HttpServletRequest request, HttpServletResponse response) throws ServletException{
		logger.info("Inside the method SearchFile, url[/SearchFile], http method[POST] :: start");
		
		ModelAndView mav = null;
		mav=new ModelAndView("home");
		String fileName=request.getParameter("searchFile");
		mav.addObject("fileName", fileName);
		mav.addObject("fileDetails", dealsService.searchFile(fileName));
		
		logger.info("Inside the method SearchFile, url[/SearchFile], http method[POST] :: end");
		return mav;
	}
	
	public void uploadFile(HttpServletRequest request){
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (isMultipart) {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			try {
				// Parse the request
				List<FileItem> items = upload.parseRequest(request);
				Iterator iterator = items.iterator();
				while (iterator.hasNext()) {
					FileItem item = (FileItem) iterator.next();
					if (!item.isFormField()) {
						String fileName = item.getName();  
						String root = request.getServletContext().getRealPath("/");
						File path = new File(root + "/uploads");
						if (!path.exists()) {
							boolean status = path.mkdirs();
						}

						File uploadedFile = new File(path + "/" + fileName);
						System.out.println(uploadedFile.getAbsolutePath());
						item.write(uploadedFile);
						filePath = uploadedFile.toString();
					}
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
	
	public void readFromCSV(String filePath){
		logger.info("Inside the method readFromCSV, read data from csv file :: Start");
		List<Deals> validList = new ArrayList<>();
		List<DealsInvalid> invalidList = new ArrayList<>();
		String fileName= getFileName(filePath);
		//fileId=dealsService.addFileData(fileName);
		System.out.println(fileName);
		try (BufferedReader br = Files.newBufferedReader(Paths.get(filePath), StandardCharsets.US_ASCII)) 
		{ 
			String line = br.readLine(); 
			while (line != null && !line.isEmpty()) { 
				String[] attributes = line.split(","); 
				createDeals(attributes,validList,invalidList);  
				line = br.readLine();
			}
			if(validList.size()>0){
				dealsService.addData(validList);
			}
			if(invalidList.size()>0){
				dealsService.addInvalidData(invalidList);
			}
			
		} 
		catch (IOException ioe) 
		{ 
			System.out.println("error");
			ioe.printStackTrace(); 
		} 
		logger.info("Inside the method readFromCSV, read data from csv file :: end");
	}
	
	public  void createDeals(String[] metadata, List<Deals> validList, List<DealsInvalid> invalidList) {
		logger.info("Inside the method createDeals, for adding deals :: Start");
		boolean validatedData = this.validate(metadata);
		if(validatedData){
			Deals validDeal = new Deals(Integer.parseInt(metadata[0]), metadata[1],metadata[2],Timestamp.valueOf(metadata[3]),Double.parseDouble(metadata[4]),fileId);
			validList.add(validDeal);
		}
		else{
			DealsInvalid inValidDeals = new DealsInvalid(Integer.parseInt(metadata[0]), metadata[1],metadata[2],Timestamp.valueOf(metadata[3]),Double.parseDouble(metadata[4]),fileId);
			invalidList.add(inValidDeals);
		}
		
		logger.info("Inside the method createDeals, for adding deals :: end");

	}
	
	
	
	public boolean validate(String[] metadata) {
		logger.info("Inside the method validate, for validate deals :: Start");
		boolean valid=false;
		String id=metadata[0];
		String fromCode=metadata[1];
		String toCode=metadata[2];
		Timestamp time= Timestamp.valueOf(metadata[3]);
		double amount=Double.parseDouble(metadata[4]);
		if((null == id || id.isEmpty() || Integer.parseInt(id)<0) || fromCode.isEmpty()||toCode.isEmpty()||time.equals(null)||time.toString().isEmpty()||String.valueOf(amount).isEmpty()){
			valid = false;
		}
		else if(!fromCode.isEmpty() && !toCode.isEmpty()) {
			try{
				boolean fCode;
				boolean tCode;
				Set<Currency> currencies = Currency.getAvailableCurrencies();
				String cur=currencies.toString();
				fCode = cur.contains(fromCode);
				tCode = cur.contains(toCode);
				if(fCode && tCode){
					valid = true;
				}
				else
				{
					
					valid = false;
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		else{
			valid = true;
		}
		logger.info("Inside the method validate, for validate deals :: end");
		return valid;
		
	}

	public String getFileName(String filePath){
		logger.info("Inside the method getFileName, for getting file name :: start");
		Path pathToFile = Paths.get(filePath); 
		String fileName=pathToFile.getFileName().toString();
		logger.info("Inside the method getFileName, for getting file name :: end");
		return fileName;
	}
}
